#!/usr/bin/env python3

import re


def clr(string):
    return None if string is '' else re.sub('\(.*\)', '', string).lower().strip()


def clean_date(date):
    if date is None or date is 0 or date is '':
        return None
    return int(date)