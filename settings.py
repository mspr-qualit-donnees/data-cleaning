#!/usr/bin/env python3

import re

genders = [
    {"id": 0, "label": "femal"},
    {"id": 1, "label": "male"},
]

locations = []
museums = []
materials = []
authors = []
domains = []
artwork_types = []
artwork_authors = []
artwork_materials = []
artwork_domains = []
artworks = []
