#!/usr/bin/env python3

import argparse
import json
import os
from to_sql import *
from extract_object import *


def filter_fields(record):
    result = {}

    fields = record["fields"]

    type_id = extract_type(fields)
    location_id = extract_location(fields)
    museum_id = extract_museum(fields, location_id)
    material_ids = extract_materials(fields)
    author_ids = extract_authors(fields)
    domain_ids = extract_domains(fields)

    artwork_id = len(artworks)

    for author_id in author_ids:
        artwork_authors.append({
            "artwork_id": artwork_id,
            "author_id": author_id
        })

    for material_id in material_ids:
        artwork_materials.append({
            "artwork_id": artwork_id,
            "material_id": material_id
        })

    for domain_id in domain_ids:
        artwork_domains.append({
            "artwork_id": artwork_id,
            "domain_id": domain_id
        })

    width, height, length = extract_dimensions(fields)

    artworks.append({
        "notice_id": clr(fields.get("ref", "")),
        "title": clr(fields.get("titr", "")),
        "creation_period": extract_period(fields),
        "width": width,
        "height": height,
        "length": length,
        "import_year": fields.get("dmis", None),
        "type_id": type_id,
        "museum_id": museum_id,
    })


def parse_args():
    parser = argparse.ArgumentParser(description="Clean the data", usage="%(prog)s <DataFile>")
    parser.add_argument("datafile", metavar="DataFile", help="CSV of the data")
    args = parser.parse_args()
    assert (os.path.isfile(args.datafile) is True), "Error: file not found"
    return args


def main():
    args = parse_args()
    with open(args.datafile, "r") as datafile:
        data = json.loads(datafile.read())

    for d in data:
        filter_fields(d)

    locations_to_sql(locations)
    domains_to_sql(domains)
    gender_to_sql(genders)
    materials_to_sql(materials)

    museums_to_sql(museums)
    authors_to_sql(authors)

    artworks_to_sql(artworks)

    artwork_types_to_sql(artwork_types)
    artwork_authors_to_sql(artwork_authors)
    artwork_materials_to_sql(artwork_materials)
    artwork_domains_to_sql(artwork_domains)

    print(json.dumps(artworks))


if __name__ == '__main__':
    main()
