--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5 (Debian 11.5-3.pgdg90+1)
-- Dumped by pg_dump version 11.5 (Debian 11.5-3.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: artwork; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.artwork (
    id integer NOT NULL,
    museum_id integer NOT NULL,
    notice_id character varying(50) NOT NULL,
    title character varying(255) NOT NULL,
    creation_period character varying(255) NOT NULL,
    length double precision NOT NULL,
    width double precision NOT NULL,
    height double precision,
    import_year integer NOT NULL,
    creation_year integer
);


ALTER TABLE public.artwork OWNER TO postgres;

--
-- Name: artwork_author; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.artwork_author (
    artwork_id integer NOT NULL,
    author_id integer NOT NULL
);


ALTER TABLE public.artwork_author OWNER TO postgres;

--
-- Name: artwork_domain; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.artwork_domain (
    artwork_id integer NOT NULL,
    domain_id integer NOT NULL
);


ALTER TABLE public.artwork_domain OWNER TO postgres;

--
-- Name: artwork_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.artwork_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.artwork_id_seq OWNER TO postgres;

--
-- Name: artwork_material; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.artwork_material (
    artwork_id integer NOT NULL,
    material_id integer NOT NULL
);


ALTER TABLE public.artwork_material OWNER TO postgres;

--
-- Name: author; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.author (
    id integer NOT NULL,
    gender_id integer NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    birth_date timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    death_date timestamp(0) without time zone DEFAULT NULL::timestamp without time zone
);


ALTER TABLE public.author OWNER TO postgres;

--
-- Name: author_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.author_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.author_id_seq OWNER TO postgres;

--
-- Name: domain; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.domain (
    id integer NOT NULL,
    label character varying(255) NOT NULL
);


ALTER TABLE public.domain OWNER TO postgres;

--
-- Name: domain_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.domain_id_seq OWNER TO postgres;

--
-- Name: gender; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gender (
    id integer NOT NULL,
    label character varying(1) NOT NULL
);


ALTER TABLE public.gender OWNER TO postgres;

--
-- Name: gender_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gender_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gender_id_seq OWNER TO postgres;

--
-- Name: location; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.location (
    id integer NOT NULL,
    city character varying(255) NOT NULL,
    zip_code character varying(20),
    latitude character varying(255) DEFAULT NULL::character varying,
    longitude character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public.location OWNER TO postgres;

--
-- Name: location_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.location_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.location_id_seq OWNER TO postgres;

--
-- Name: material; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.material (
    id integer NOT NULL,
    label character varying(255) NOT NULL
);


ALTER TABLE public.material OWNER TO postgres;

--
-- Name: material_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.material_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.material_id_seq OWNER TO postgres;

--
-- Name: migration_versions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migration_versions (
    version character varying(14) NOT NULL,
    executed_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.migration_versions OWNER TO postgres;

--
-- Name: COLUMN migration_versions.executed_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.migration_versions.executed_at IS '(DC2Type:datetime_immutable)';


--
-- Name: museum; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.museum (
    id integer NOT NULL,
    location_id integer NOT NULL,
    name character varying(255) NOT NULL,
    code character varying(50) NOT NULL
);


ALTER TABLE public.museum OWNER TO postgres;

--
-- Name: museum_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.museum_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.museum_id_seq OWNER TO postgres;

--
-- Name: type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.type_id_seq OWNER TO postgres;

--
-- Data for Name: artwork; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.artwork (id, museum_id, notice_id, title, creation_period, length, width, height, import_year, creation_year) FROM stdin;
\.


--
-- Data for Name: artwork_author; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.artwork_author (artwork_id, author_id) FROM stdin;
\.


--
-- Data for Name: artwork_domain; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.artwork_domain (artwork_id, domain_id) FROM stdin;
\.


--
-- Data for Name: artwork_material; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.artwork_material (artwork_id, material_id) FROM stdin;
\.


--
-- Data for Name: author; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.author (id, gender_id, first_name, last_name, birth_date, death_date) FROM stdin;
\.


--
-- Data for Name: domain; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.domain (id, label) FROM stdin;
\.


--
-- Data for Name: gender; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.gender (id, label) FROM stdin;
\.


--
-- Data for Name: location; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.location (id, city, zip_code, latitude, longitude) FROM stdin;
\.


--
-- Data for Name: material; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.material (id, label) FROM stdin;
\.


--
-- Data for Name: migration_versions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migration_versions (version, executed_at) FROM stdin;
20191030111536	2019-10-30 16:10:55
20191030143650	2019-10-30 16:10:55
20191030222131	2019-10-30 22:23:50
\.


--
-- Data for Name: museum; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.museum (id, location_id, name, code) FROM stdin;
\.


--
-- Name: artwork_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.artwork_id_seq', 1, false);


--
-- Name: author_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.author_id_seq', 1, false);


--
-- Name: domain_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.domain_id_seq', 1, false);


--
-- Name: gender_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gender_id_seq', 1, false);


--
-- Name: location_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.location_id_seq', 1, false);


--
-- Name: material_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.material_id_seq', 1, false);


--
-- Name: museum_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.museum_id_seq', 1, false);


--
-- Name: type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.type_id_seq', 1, false);


--
-- Name: artwork_author artwork_author_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.artwork_author
    ADD CONSTRAINT artwork_author_pkey PRIMARY KEY (artwork_id, author_id);


--
-- Name: artwork_domain artwork_domain_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.artwork_domain
    ADD CONSTRAINT artwork_domain_pkey PRIMARY KEY (artwork_id, domain_id);


--
-- Name: artwork_material artwork_material_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.artwork_material
    ADD CONSTRAINT artwork_material_pkey PRIMARY KEY (artwork_id, material_id);


--
-- Name: artwork artwork_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.artwork
    ADD CONSTRAINT artwork_pkey PRIMARY KEY (id);


--
-- Name: author author_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.author
    ADD CONSTRAINT author_pkey PRIMARY KEY (id);


--
-- Name: domain domain_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.domain
    ADD CONSTRAINT domain_pkey PRIMARY KEY (id);


--
-- Name: gender gender_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gender
    ADD CONSTRAINT gender_pkey PRIMARY KEY (id);


--
-- Name: location location_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.location
    ADD CONSTRAINT location_pkey PRIMARY KEY (id);


--
-- Name: material material_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.material
    ADD CONSTRAINT material_pkey PRIMARY KEY (id);


--
-- Name: migration_versions migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migration_versions
    ADD CONSTRAINT migration_versions_pkey PRIMARY KEY (version);


--
-- Name: museum museum_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.museum
    ADD CONSTRAINT museum_pkey PRIMARY KEY (id);


--
-- Name: idx_405ccbdadb8ffa4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_405ccbdadb8ffa4 ON public.artwork_author USING btree (artwork_id);


--
-- Name: idx_405ccbdaf675f31b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_405ccbdaf675f31b ON public.artwork_author USING btree (author_id);


--
-- Name: idx_5a5a0d19115f0ee5; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_5a5a0d19115f0ee5 ON public.artwork_domain USING btree (domain_id);


--
-- Name: idx_5a5a0d19db8ffa4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_5a5a0d19db8ffa4 ON public.artwork_domain USING btree (artwork_id);


--
-- Name: idx_6247447764d218e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_6247447764d218e ON public.museum USING btree (location_id);


--
-- Name: idx_80f4b96bdb8ffa4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_80f4b96bdb8ffa4 ON public.artwork_material USING btree (artwork_id);


--
-- Name: idx_80f4b96be308ac6f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_80f4b96be308ac6f ON public.artwork_material USING btree (material_id);


--
-- Name: idx_881fc5764b52e5b5; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_881fc5764b52e5b5 ON public.artwork USING btree (museum_id);


--
-- Name: idx_bdafd8c8708a0e0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_bdafd8c8708a0e0 ON public.author USING btree (gender_id);


--
-- Name: uniq_6247447777153098; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uniq_6247447777153098 ON public.museum USING btree (code);


--
-- Name: uniq_881fc5767d540ab; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uniq_881fc5767d540ab ON public.artwork USING btree (notice_id);


--
-- Name: artwork_author fk_405ccbdadb8ffa4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.artwork_author
    ADD CONSTRAINT fk_405ccbdadb8ffa4 FOREIGN KEY (artwork_id) REFERENCES public.artwork(id) ON DELETE CASCADE;


--
-- Name: artwork_author fk_405ccbdaf675f31b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.artwork_author
    ADD CONSTRAINT fk_405ccbdaf675f31b FOREIGN KEY (author_id) REFERENCES public.author(id) ON DELETE CASCADE;


--
-- Name: artwork_domain fk_5a5a0d19115f0ee5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.artwork_domain
    ADD CONSTRAINT fk_5a5a0d19115f0ee5 FOREIGN KEY (domain_id) REFERENCES public.domain(id) ON DELETE CASCADE;


--
-- Name: artwork_domain fk_5a5a0d19db8ffa4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.artwork_domain
    ADD CONSTRAINT fk_5a5a0d19db8ffa4 FOREIGN KEY (artwork_id) REFERENCES public.artwork(id) ON DELETE CASCADE;


--
-- Name: museum fk_6247447764d218e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.museum
    ADD CONSTRAINT fk_6247447764d218e FOREIGN KEY (location_id) REFERENCES public.location(id);


--
-- Name: artwork_material fk_80f4b96bdb8ffa4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.artwork_material
    ADD CONSTRAINT fk_80f4b96bdb8ffa4 FOREIGN KEY (artwork_id) REFERENCES public.artwork(id) ON DELETE CASCADE;


--
-- Name: artwork_material fk_80f4b96be308ac6f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.artwork_material
    ADD CONSTRAINT fk_80f4b96be308ac6f FOREIGN KEY (material_id) REFERENCES public.material(id) ON DELETE CASCADE;


--
-- Name: artwork fk_881fc5764b52e5b5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.artwork
    ADD CONSTRAINT fk_881fc5764b52e5b5 FOREIGN KEY (museum_id) REFERENCES public.museum(id);


--
-- Name: author fk_bdafd8c8708a0e0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.author
    ADD CONSTRAINT fk_bdafd8c8708a0e0 FOREIGN KEY (gender_id) REFERENCES public.gender(id);


--
-- PostgreSQL database dump complete
--

