#!/bin/bash

set -e

psql -v --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" < /srv/db.sql
