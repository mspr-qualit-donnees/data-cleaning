#!/usr/bin/env python3

from settings import *
from get_object_id import *
from create_object import *


def extract_type(fields):
    libelle = fields.get("repr", "").split('(')[0]
    type_id = get_artwork_id(libelle)
    if len(artwork_types) == 0 or type_id == len(artwork_types):
        create_type(type_id, libelle)
    return type_id


def extract_location(fields):
    city_name = fields.get("loca", "").split(';')[0]
    longitude = str(fields.get("geolocalisation_ville", [0, 0])[0])
    latitude = str(fields.get("geolocalisation_ville", [0, 0])[1])
    loc_id = get_location_id(city_name, latitude, longitude)
    if len(locations) == 0 or loc_id == len(locations):
        create_location(loc_id, city_name, latitude, longitude)
    return loc_id


def extract_museum(fields, location_id):
    museum_code = fields.get("museo", "")
    museum_name = fields.get("loca", "").split(';')[1]
    museum_id = get_museum_id(museum_code, museum_name, location_id)
    if len(museums) == 0 or museum_id == len(museums):
        create_museum(museum_id, museum_code, museum_name, location_id)
    return museum_id


def extract_materials(fields):
    material_labels = fields.get("tech", "").split(';')
    ids = []
    for label in material_labels:
        mat_id = get_material_id(label)
        ids.append(mat_id)
        if len(materials) == 0 or mat_id == len(materials):
            create_material(mat_id, label)
    return ids


def extract_author_dates(fields):
    dates = fields.get("paut", "").split(';')
    dates = [re.sub('[^\d]', '', date) for date in dates]
    while len(dates) < 2:
        dates.append(None)
    return tuple(dates[:2])


def extract_authors(fields):
    ids = []
    for author in fields.get("autr", "").split(';'):
        first_name = []
        last_name = []
        for name in author.split(' '):
            if name.isupper():
                last_name.append(name)
            else:
                first_name.append(name)
        first_name = ' '.join(first_name)
        last_name = ' '.join(last_name)
        birth, death = extract_author_dates(fields)
        aut_id = get_author_id(first_name, last_name)
        ids.append(aut_id)
        if len(authors) == 0 or aut_id == len(authors):
            create_author(aut_id, 0, first_name, last_name, birth, death)
    return ids


def extract_domains(fields):
    ids = []
    domain_labels = fields.get("domn", "").split(';')
    for label in domain_labels:
        domain_id = get_domain_id(label)
        ids.append(domain_id)
        if len(domains) == 0 or domain_id == len(domains):
            create_domain(domain_id, label)
    return ids


def extract_dimensions(fields):
    dimensions = fields.get("dims", "").split(';')
    result = []
    for d in dimensions:
        d = re.sub('[^\d]', '', d)
        result.append(int(0 if d is '' else d))
    while len(result) < 3:
        result.append(0)
    return tuple(result[:3])


def extract_period(fields):
    period = fields.get("peri", "").split(';')[0]
    period = re.sub('[^\d]', ' ', period).split(' ')
    b = []
    for x in period:
        if x is not '':
            b.append(x)
    if len(b) == 1:
        century = int(b[0])
        quarter = 0
    else:
        quarter = int(b[0])
        century = int(b[1])
    date = ((century - 1) * 100) + (quarter * 25)
    return date
