#!/usr/bin/env python3

from settings import *
from tools import *


def create_type(type_id, libelle):
    artwork_types.append({
        "id": type_id,
        "label": clr(libelle)
    })


def create_location(city_id, name, latitude, longitude):
    locations.append({
        "id": city_id,
        "city": clr(name),
        "latitude": clr(latitude),
        "longitude": clr(longitude)
    })


def create_museum(museum_id, museum_code, museum_name, loc_id):
    museums.append({
        "id": museum_id,
        "location": loc_id,
        "name": clr(museum_name),
        "code": clr(museum_code)
    })


def create_material(material_id, label):
    materials.append({
        "id": material_id,
        "label": clr(label)
    })


def create_author(author_id, gender_id, first_name, last_name, birth, death):
    authors.append({
        "id": author_id,
        "gender_id": gender_id,
        "first_name": clr(first_name),
        "last_name": clr(last_name),
        "birth_date": clean_date(birth),
        "death_date": clean_date(death)
    })


def create_domain(domain_id, label):
    domains.append({
        "id": domain_id,
        "label": clr(label)
    })


