#!/usr/bin/env python3

from settings import *
from tools import clr


def get_artwork_id(libelle):
    for value in artwork_types:
        if value["label"] == clr(libelle):
            return value["id"]
    return len(artwork_types)


def get_location_id(city_name, latitude, longitude):
    for value in locations:
        if value["city"] == clr(city_name) \
                and value["longitude"] == clr(longitude) \
                and value["latitude"] == clr(latitude):
            return value["id"]
    return len(locations)


def get_museum_id(museum_code, museum_name, location_id):
    for value in museums:
        if value["location"] == location_id \
                and value["code"] == clr(museum_code) \
                and value["name"] == clr(museum_name):
            return value["id"]
    return len(museums)


def get_material_id(label):
    for value in materials:
        if value["label"] == clr(label):
            return value['id']
    return len(materials)


def get_author_id(first_name, last_name):
    for value in authors:
        if value["first_name"] == clr(first_name) \
                and value["last_name"] == clr(last_name):
            return value["id"]
    return len(authors)


def get_domain_id(label):
    for value in domains:
        if value["label"] == clr(label):
            return value["id"]
    return len(domains)